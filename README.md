# schedules

Schedules können benutzt werden, um Pipelines automatisch zu bestimmten Zeiten anzustoßen.
In einem GitLab-Projekt können Schedules unter `CI / CD` -> `Schedules` konfiguriert werden.

Weitere Informationen siehe: https://docs.gitlab.com/ee/ci/pipelines/schedules.html
